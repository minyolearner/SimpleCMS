
    <!-- Post Section -->
    <section class="post">

        <article class="container">
            <div class="row">
                <div class="column">
                    <img src="https://picsum.photos/200" alt="Lorem Picsum">
                </div>
    
                <div class="column column-75">
                    <h2>Blog Post Title</h2>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab, fuga! Libero at aperiam optio inventore, animi repellendus atque! Dolor exercitationem reprehenderit totam tempora libero. Ab corrupti praesentium consequuntur quam commodi.</p>
                    <a href="#" class="button button-outline">Read</a>
                </div
            </div>
         </article>
         <!-- Pagenation -->
         <div class="page container clearfix">
            <div class="float-right">
                <a href="#" class="button button-outline"> Prev </a>
                <a href="#" class="button button-outline"> Next </a>
            </div>
         </div>
         
    </section>