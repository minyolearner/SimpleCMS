# SimpleCMS

# Docker Setup

To run this stack, navigate to the directory where the `docker-compose.yml` file is located and run `docker-compose up -d`. This will start the two services in the background. You can then access your PHP application by going to http://localhost in your web browser.

