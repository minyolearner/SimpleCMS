<?php

include "../header.php";

include "admin-nav.php";
?>


    <section class="main clearfix container">

        <div class="row">
        <div class="float-left column column-25">
            <a href="" class="button button-primary">New Post</a> <br>
            <a href="" class="button button-primary">All Post</a> <br>
            <a href="" class="button button-primary">Settings</a> <br>
        </div>

        <div class="float-right column column-75">
            <form action="post">
                <fieldset>
                    <input type="text" name="header" id="header">
                    <textarea name="main-content" id="main-content" cols="60" rows="500"></textarea>
                    <input type="file" name="image" id="image">
                    <br>
                    <input type="submit">
                </fieldset>
            </form>
        </div>
        </div>


    </section>

<?php

include "../footer.php";

?>