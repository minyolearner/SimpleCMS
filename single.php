<?php
include "header.php";

include "nav.php";
?>

    <!-- Post Section -->
    <section class="post">

        <article class="container">

                <div class="">
                    <img src="https://picsum.photos/600/300" alt="Lorem Picsum">
                </div>
    
                <div class="">
                    <h2>Blog Post Title</h2>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab, fuga! Libero at aperiam optio inventore, animi repellendus atque! Dolor exercitationem reprehenderit totam tempora libero. Ab corrupti praesentium consequuntur quam commodi.</p>   
                </div

         </article>
         
    </section>

<?php

include "footer.php";

?>