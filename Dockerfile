FROM php:7.4-fpm

# Install necessary PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Copy application files
COPY index.php /var/www/html/

# Set up permissions for the application files
RUN chown -R www-data:www-data /var/www/html/

EXPOSE 9000
CMD ["php-fpm"]
